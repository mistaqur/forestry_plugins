package mistaqur.forestry;

import forestry.api.cultivation.ICropEntity;
import forestry.api.cultivation.ICropProvider;

import forestry.plugins.PluginIC2Crops;

import net.minecraft.src.Block;
import net.minecraft.src.Item;
import net.minecraft.src.ItemStack;
import net.minecraft.src.NBTTagCompound;
import net.minecraft.src.TileEntity;
import net.minecraft.src.World;
import ic2.api.TECrop;
import ic2.api.CropCard;
import ic2.api.BaseSeed;



public class CropProviderIC2 implements ICropProvider {

	@Override
	public boolean isGermling(ItemStack germling) {
		if (germling.itemID == Item.seeds.shiftedIndex || germling.itemID == Item.melonSeeds.shiftedIndex || germling.itemID == Item.pumpkinSeeds.shiftedIndex)
			return false;
		if (CropCard.getBaseSeed(germling) != null)
			return true;
		return germling.itemID == PluginIC2Crops.crop.itemID || germling.itemID == PluginIC2Crops.cropSeed.itemID;
	}

	@Override
	public boolean isCrop(World world, int x, int y, int z) {
		int blockid = world.getBlockId(x, y, z);
		return blockid == PluginIC2Crops.crop.itemID;
	}

	@Override
	public ItemStack[] getWindfall() {
		return new ItemStack[] {PluginIC2Crops.cropSeed};
	}
	public BaseSeed getBaseSeedFromBag(ItemStack bag) {
		NBTTagCompound tags = bag.getTagCompound();
		if (tags == null)
			return null;
		return new BaseSeed(tags.getShort("id"),1,tags.getByte("growth"),tags.getByte("gain"),tags.getByte("resistance"),1);
	}
	public int getScanFromBag(ItemStack bag) {
		NBTTagCompound tags = bag.getTagCompound();
		if (tags == null)
			return -1;
		return tags.getByte("scan");
	}

	@Override
	public boolean doPlant(ItemStack germling, World world, int x, int y, int z) {
		int blockid = world.getBlockId(x, y, z);
		if (germling.itemID == PluginIC2Crops.crop.itemID) {

			// Target block needs to be empty
			if (blockid != 0)
				return false;

			// Can only plant on soulsand
			int below = world.getBlockId(x, y - 1, z);
			if (below != Block.tilledField.blockID)
				return false;

			world.setBlockAndMetadataWithNotify(x, y, z, PluginIC2Crops.crop.itemID, 0);
			return true;
		} else {
			if (blockid != PluginIC2Crops.crop.itemID)
				return false;
			TileEntity entity = world.getBlockTileEntity(x, y, z);
			if (entity == null || !(entity instanceof TECrop))
				return false;
			TECrop te = (TECrop) entity;
			if (te.id > -1)
				return false;
			BaseSeed seed = null;
			int scan = 1;
			if (germling.itemID == PluginIC2Crops.cropSeed.itemID) {
				seed = getBaseSeedFromBag(germling);
				scan = getScanFromBag(germling);
			} else {
				seed = CropCard.getBaseSeed(germling);
			}

			if (seed == null || seed.stackSize > germling.stackSize)
				return false;
			CropCard t = CropCard.getCrop(seed.id);
			if (t.canGrow(te)) {
				te.reset();
				te.id = (short)seed.id;
				te.size = (byte)seed.size;
				te.statGrowth = (byte)seed.statGrowth;
				te.statGain = (byte)seed.statGain;
				te.statResistance = (byte)seed.statResistance;
				te.scanLevel = (byte)scan;
				if (seed.stackSize > 1)
					germling.stackSize -= seed.stackSize - 1;
				return true;
			}
			return false;			
		}
	}

	@Override
	public ICropEntity getCrop(World world, int x, int y, int z) {
		return new CropIC2(world, x, y, z);
	}

}
