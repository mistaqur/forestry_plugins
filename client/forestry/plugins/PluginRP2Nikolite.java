package forestry.plugins;

import java.util.Random;
import java.io.File;

import forestry.api.core.IAchievementHandler;
import forestry.api.core.IPlugin;
import forestry.api.core.IResupplyHandler;
import forestry.api.core.ItemInterface;
import forestry.api.cultivation.CropProviders;
import forestry.api.recipes.IGenericCrate;
import forestry.api.recipes.RecipeManagers;

import net.minecraft.src.Block;
import net.minecraft.src.ItemStack;
import net.minecraft.src.Item;
import net.minecraft.src.ModLoader;
import net.minecraft.src.World;

import net.minecraft.src.forge.IGuiHandler;
import net.minecraft.src.forge.IPacketHandler;
import net.minecraft.src.forge.IPickupHandler;

import net.minecraft.client.Minecraft;
import net.minecraft.src.forge.Configuration;
import net.minecraft.src.forge.Property;

public class PluginRP2Nikolite implements IPlugin {

	protected int id = 0;
	protected int meta = 0;

	@Override
	public boolean isAvailable() {
		return ModLoader.isModLoaded("mod_RedPowerCore");
	}

	@Override
	public void doInit() {

	}

	@Override
	public String getDescription() {
		return "Crated nikolite";
	}

	@Override
	public void preInit() {
		Configuration config = new Configuration(new File(Minecraft.getMinecraftDir(), "config/forestry/rp2nikolite.conf"));
		config.load();
		Property t = config.getOrCreateProperty("Nikolite", "general", "1257:6");
		String[] ident = t.value.split("[:]+");
		if (ident.length > 1) {
			id = Integer.parseInt(ident[0]);
			meta = Integer.parseInt(ident[1]);
		} else {
			id = Integer.parseInt(ident[0]);
		}
		config.save();
	}

	@Override
	public void postInit() {
		ItemStack q = ItemInterface.getItem("cratedNikolite");
		if (q == null) {
			ModLoader.getLogger().warning("cratedNikolite item is null");
		}
		Item cratedNikolite = q.getItem();
		IGenericCrate t = (IGenericCrate)cratedNikolite;
		if (t.getContained(q) != null) {
			ModLoader.getLogger().warning("cratedNikolite already have contained item");
		} else if (Item.itemsList[id] != null ) {
			ItemStack ore = new ItemStack(id, 1, meta);
			t.setContained(q, ore);
			if (RecipeManagers.carpenterManager != null)
				RecipeManagers.carpenterManager.addCrating(ore, q);
		} else {
			ModLoader.getLogger().warning("Nikolite item " + id + ":" + meta + " not found");
		}
	}

	@Override
	public void generateSurface(World world, Random rand, int chunkX, int chunkZ) {
	};

	@Override
	public IGuiHandler getGuiHandler() {
		return null;
	}

	@Override
	public IPacketHandler getPacketHandler() {
		return null;
	}

	@Override
	public IPickupHandler getPickupHandler() {
		return null;
	}

	@Override
	public IAchievementHandler getAchievementHandler() {
		return null;
	}

	@Override
	public IResupplyHandler getResupplyHandler() {
		return null;
	}

}
